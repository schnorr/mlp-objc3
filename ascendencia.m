#include <Foundation/Foundation.h>

int main (int argc, char **argv)
{
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  NSFileHandle *input = [NSFileHandle fileHandleWithStandardInput];
  NSData *inputData = [NSData dataWithData:[input availableData]];
  NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];

  NSLog (@"=> %@", inputString);
  //escreva seu codigo a partir daqui
  //o nome da classe se encontra na variável inputString

  [pool release];
  return 0;
}
