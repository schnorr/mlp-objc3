#include <Foundation/Foundation.h>

int main (int argc, char **argv)
{
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  //Obter uma referencia para uma class cujo nome eh NSObject
  Class umaClasse = NSClassFromString(@"NSObject");

  //Obter uma referencia para um metodo cujo nome eh description
  SEL umMetodo = NSSelectorFromString(@"description");

  //Instanciar um objeto da classe
  id objeto = [[umaClasse alloc] init];

  //Chamar um metodo do objeto instanciado
  id resultado = [objeto performSelector: umMetodo];

  //Imprimir o resultado, seja ele qual for
  NSLog (@"%@", resultado);

  //Verificar a existência de um protocolo
  Protocol *protocolo = NSProtocolFromString(@"NSCopying");
  if (protocolo){
    NSLog (@"protocolo %@ existe", NSStringFromProtocol(protocolo));
  }

  //Verificar se o objeto de umaClasse segue o protocolo NSCopying
  if ([objeto conformsToProtocol: protocolo]){
    NSLog (@"objeto %@ segue o protocolo %@", objeto, NSStringFromProtocol(protocolo));
  }

  [pool release];
  return 0;
}
